public class Player {

    private String name;
    private int money;
    private CardCollection playerCards = new CardCollection();
    private boolean playerStatus;
    private int betAmount;

    public Player(String name)
    {
        this.name = name;
    }

    public void receiveCard(Card card)
    {
        playerCards.addCard(card);
    }
    public void getValueOfHand()
    {
        playerCards.calculateValueOfHand();
        playerCards.display();
    }
    public void printCard()
    {
        playerCards.printCard();
    }

    public static void main(String args[])
    {
        Player playerTest = new Player("Mai");
        DeckCard deckcards = DeckCard.getInstance();
        deckcards.initialize();
        for(int i = 0;i<5;i++)
        {
            playerTest.receiveCard(deckcards.drawFromDeck());
        }
        playerTest.printCard();
        playerTest.getValueOfHand();


    }

}
