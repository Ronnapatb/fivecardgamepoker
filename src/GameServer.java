import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class GameServer {
    //initialize socket and input stream
    private ArrayList<PlayerProxy> playerList = new ArrayList<>();
    private ServerSocket server = null;
    private ObjectInputStream in = null;

    // constructor with port
    public GameServer(int port)
    {
        // starts server and waits for a connection
        try
        {
            server = new ServerSocket(port);
            System.out.println("Server started");

            System.out.println("Waiting for a client ...");

            Socket socket = server.accept();
            System.out.println("Client accepted");

            // takes input from the client socket
            in = new ObjectInputStream(socket.getInputStream());

            Object line = null;

            // reads message from client until "Over" is sent
            while (!line.equals("Over"))
            {
                try
                {
                    line = in.readObject();;
                    System.out.println(line);


                }
                catch(IOException i)
                {
                    System.out.println(i);
                }
            }
            System.out.println("Closing connection");

            // close connection
            socket.close();
            in.close();
        }
        catch(IOException i)
        {
            System.out.println(i);
        }
    }

    public static void main(String args[])
    {
//        ServerProxy server = new ServerProxy(5000);
//        int players = 0;
//        while ((players < 2) || (players > 4))
//        {
//            players = IOFuns.getOneInteger("Enter the how many playter?(Enter 2-5): ");
//        }
        Server server = new Server(5000);
//        System.out.println(players);
    }
}

