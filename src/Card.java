public class Card {
    private int rank;
    private int suit;

    private static String[] suits = { "H", "S", "D", "C" };
    private static String[] ranks  = { "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King" };


    Card(int suit, int rank)
    {
        this.rank=rank;
        this.suit=suit;
    }

    public static String rankAsString( int __rank )
    {
        return ranks[__rank];
    }

    //@Override
    public  String toString()
    {
        return ranks[rank] + ":" + suits[suit];
    }

    public int getRank()
    {
        return rank;
    }

    public int getSuit()
    {
        return suit;
    }
}
