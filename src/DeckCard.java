import java.util.Random;
import java.util.ArrayList;


public class DeckCard {
    private static DeckCard deckInstance = new DeckCard();
    private ArrayList<Card> deckCards = new ArrayList<Card>();

    private DeckCard()
    {

    }

    public void initialize()
    {
        for (int a=0; a<=3; a++)
        {
            for (int b=0; b<=12; b++)
            {
                deckCards.add(new Card(a,b));
            }
        }
        shuffleCard();
    }
    public static DeckCard getInstance()
    {
        return deckInstance;
    }

    public void shuffleCard()
    {
        int size = deckCards.size() -1;
        int index_1;
        int index_2;
        Random generator = new Random();
        Card temp;

        for (int i=0; i< 100; i++)
        {
            index_1 = generator.nextInt( size );
            index_2 = generator.nextInt( size );

            temp = (Card) deckCards.get( index_2 );
            deckCards.set( index_2 , deckCards.get( index_1 ) );
            deckCards.set( index_1, temp );
        }
    }

    public Card drawFromDeck()
    {
        return deckCards.remove(deckCards.size()-1);
    }

    public void printDeckCard()
    {
        System.out.println(this.deckCards.size());
        for(int i = 0; i< this.deckCards.size();i++)
        {
            System.out.println(this.deckCards.get(i));
        }
    }

    public static void main(String[] args)
    {
        deckInstance.initialize();
        deckInstance.printDeckCard();
    }
}
