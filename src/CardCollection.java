import java.util.ArrayList;

public class CardCollection {

    private ArrayList<Card> cards = new ArrayList<>();
    private int[] valueOfHand;

    public CardCollection()
    {
        valueOfHand = new int[6];
    }

    public void printCard()
    {
        for(int i = 0;i<getCardCount();i++)
        {
            System.out.println(cards.get(i));
        }
    }
    public int getCardCount()
    {
        return cards.size();
    }
    public void addCard(Card card)
    {
        cards.add(card);
    }
    public void removeCard(Card card)
    {
        int selectedCard;
        selectedCard = cards.indexOf(card);
        cards.remove(selectedCard);
    }
    public void sortCard()
    {

    }
    public void calculateValueOfHand(){
//        value = new int[6];
//        cards = new Card[5];
//        for (int x=0; x<5; x++)
//        {
//            cards[x] = d.drawFromDeck();
//        }

        int[] ranks = new int[14];
        int[] orderedRanks = new int[5];
        boolean flush=true;
        boolean straight=false;
        int sameCards=1;
        int sameCards2=1;
        int largeGroupRank=0;
        int smallGroupRank=0;
        int index=0;
        int topStraightValue=0;

        for (int x=0; x<=13; x++)
        {
            ranks[x]=0;
        }
        for (int x=0; x<=4; x++)
        {
            ranks[ cards.get(x).getRank() ]++;
        }
        for (int x=0; x<4; x++) {
            if ( cards.get(x).getSuit() != cards.get(x+1).getSuit() )
                flush=false;
        }
        for (int x=13; x>=1; x--)
        {
            if (ranks[x] > sameCards)
            {
                if (sameCards != 1)  //if sameCards was not the default value
                {
                    sameCards2 = sameCards;
                    smallGroupRank = largeGroupRank;
                }

                sameCards = ranks[x];
                largeGroupRank = x;

            } else if (ranks[x] > sameCards2)
            {
                sameCards2 = ranks[x];
                smallGroupRank = x;
            }
        }


        if (ranks[1]==1) //if ace, run this before because ace is highest card
        {
            orderedRanks[index]=14;
            index++;
        }

        for (int x=13; x>=2; x--)
        {
            if (ranks[x]==1)
            {
                orderedRanks[index]=x; //if ace
                index++;
            }
        }
        for (int x=1; x<=9; x++) //can't have straight with lowest value of more than 10
        {
            if (ranks[x]==1 && ranks[x+1]==1 && ranks[x+2]==1 && ranks[x+3]==1 && ranks[x+4]==1)
            {
                straight=true;
                topStraightValue=x+4; //4 above bottom value
                break;
            }
        }
        if (ranks[10]==1 && ranks[11]==1 && ranks[12]==1 && ranks[13]==1 && ranks[1]==1) //ace high
        {
            straight=true;
            topStraightValue=14; //higher than king
        }

        for (int x=0; x<=5; x++)
        {
            valueOfHand[x]=0;
        }


        //start hand evaluation
        if ( sameCards==1 ) {
            valueOfHand[0]=1;
            valueOfHand[1]=orderedRanks[0];
            valueOfHand[2]=orderedRanks[1];
            valueOfHand[3]=orderedRanks[2];
            valueOfHand[4]=orderedRanks[3];
            valueOfHand[5]=orderedRanks[4];
        }

        if (sameCards==2 && sameCards2==1)
        {
            valueOfHand[0]=2;
            valueOfHand[1]=largeGroupRank; //rank of pair
            valueOfHand[2]=orderedRanks[0];
            valueOfHand[3]=orderedRanks[1];
            valueOfHand[4]=orderedRanks[2];
        }

        if (sameCards==2 && sameCards2==2) //two pair
        {
            valueOfHand[0]=3;
            valueOfHand[1]= largeGroupRank>smallGroupRank ? largeGroupRank : smallGroupRank; //rank of greater pair
            valueOfHand[2]= largeGroupRank<smallGroupRank ? largeGroupRank : smallGroupRank;
            valueOfHand[3]=orderedRanks[0];  //extra card
        }

        if (sameCards==3 && sameCards2!=2)
        {
            valueOfHand[0]=4;
            valueOfHand[1]= largeGroupRank;
            valueOfHand[2]=orderedRanks[0];
            valueOfHand[3]=orderedRanks[1];
        }

        if (straight && !flush)
        {
            valueOfHand[0]=5;
            valueOfHand[1]=topStraightValue;
        }

        if (flush && !straight)
        {
            valueOfHand[0]=6;
            valueOfHand[1]=orderedRanks[0]; //tie determined by ranks of cards
            valueOfHand[2]=orderedRanks[1];
            valueOfHand[3]=orderedRanks[2];
            valueOfHand[4]=orderedRanks[3];
            valueOfHand[5]=orderedRanks[4];
        }

        if (sameCards==3 && sameCards2==2)
        {
            valueOfHand[0]=7;
            valueOfHand[1]=largeGroupRank;
            valueOfHand[2]=smallGroupRank;
        }

        if (sameCards==4)
        {
            valueOfHand[0]=8;
            valueOfHand[1]=largeGroupRank;
            valueOfHand[2]=orderedRanks[0];
        }

        if (straight && flush)
        {
            valueOfHand[0]=9;
            valueOfHand[1]=topStraightValue;
        }
    }
    public int[] getValueOfHand()
    {
        return this.valueOfHand;
    }
    void display()
    {
        String s;
        System.out.println(valueOfHand[0]);
        switch( valueOfHand[0] )
        {

            case 1:
                s="high card";
                break;
            case 2:
                s="pair of " + Card.rankAsString(valueOfHand[1]) + "\'s";
                break;
            case 3:
                s="two pair " + Card.rankAsString(valueOfHand[1]) + " " + Card.rankAsString(valueOfHand[2]);
                break;
            case 4:
                s="three of a kind " + Card.rankAsString(valueOfHand[1]) + "\'s";
                break;
            case 5:
                s=Card.rankAsString(valueOfHand[1]) + " high straight";
                break;
            case 6:
                s="flush";
                break;
            case 7:
                s="full house " + Card.rankAsString(valueOfHand[1]) + " over " + Card.rankAsString(valueOfHand[2]);
                break;
            case 8:
                s="four of a kind " + Card.rankAsString(valueOfHand[1]);
                break;
            case 9:
                s="straight flush " + Card.rankAsString(valueOfHand[1]) + " high";
                break;
            default:
                s="error in Hand.display: value[0] contains invalid value";
        }
//        s = "				" + s;
        System.out.println(s);
    }

    void displayAll()
    {
        for (int x=0; x<5; x++)
            System.out.println(cards.get(x));
    }




}
